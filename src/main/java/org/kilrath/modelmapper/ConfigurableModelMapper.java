package org.kilrath.modelmapper;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import org.kilrath.modelmapper.converter.AbstractTypeConverter;
import org.kilrath.modelmapper.converter.LombokBuilderConverter;
import org.kilrath.modelmapper.converter.NumberConverter;
import org.kilrath.modelmapper.converter.NumberStringConverter;
import org.kilrath.modelmapper.converter.OptionalConverter;
import org.kilrath.modelmapper.converter.ReqArgsConstructorProvider;
import org.kilrath.modelmapper.converter.TypedValueConverter;
import org.modelmapper.ConfigurationException;
import org.modelmapper.MappingException;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.modelmapper.config.Configuration;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.convention.NamingConventions;
import org.modelmapper.spi.ConditionalConverter;
import org.modelmapper.spi.MappingContext;

public final class ConfigurableModelMapper {

    private ModelMapper mapper;

    public ConfigurableModelMapper() {
        mapper = new ModelMapper();
    }

    public ConfigurableModelMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public <T> T map(Object source, Class<T> destination) throws ConfigurationException,
            MappingException {

        Type destinationType = TypeToken.<T> of(destination).getType();

        return map(source, destinationType);

    }

    @SuppressWarnings({ "unchecked", "null" })
    public <T> T map(Object source, Type destinationType) throws ConfigurationException,
            MappingException {

        if (source == null) {
            Class<T> destination = TypeToken.<T> of(destinationType).getRawType();
            if (Optional.class.isAssignableFrom(destination)) {
                return (T) Optional.<T> empty();
            }
            return null;
        }

        T result = mapper.map(source, destinationType);
        mapper.validate();

        return result;
    }

    /**
     * Map fields from {@link Number} to {@link String} and vice versa.
     * 
     * @param format
     * 
     * @return
     */
    public ConfigurableModelMapper withNumberConverter(Optional<DecimalFormat> format) {

        List<ConditionalConverter<?, ?>> converters = mapper.getConfiguration().getConverters();

        converters.removeIf(converter -> converter.getClass().getSimpleName().equals(
                "NumberConverter"));

        converters.add(0, format.isPresent() ? new NumberConverter(
                format.get()) : new NumberConverter());

        converters.add(0, format.isPresent() ? new NumberStringConverter(
                format.get()) : new NumberStringConverter());

        return this;

    }

    /**
     * Map {@Optional} sources to {@Optional} destination types. Only source or
     * destinationType have to be an {@Optional}, but both is also possible.
     * 
     * @return
     */
    public <D> ConfigurableModelMapper withOptionalConverter() {
        mapper.getConfiguration().getConverters().add(0, new OptionalConverter());

        return this;
    }

    /**
     * Map abstract sources or abstract destination types to concrete
     * implementations. If there are multiple implementations the name of the
     * concrete source have to be part of the concrete destination type or vice
     * versa, otherwise first implementation is used.
     * 
     * @param classpathPrefix
     *            used to shrink path for classpath scanning to find concrete
     *            classes extending given abstract classes
     * 
     * @return
     */
    public <D> ConfigurableModelMapper withAbstractTypeConverter(@NotNull String classpathPrefix) {
        mapper.getConfiguration().getConverters().add(0, new AbstractTypeConverter(
                classpathPrefix, mapper.getConfiguration()));

        return this;
    }

    /**
     * Map destination types being generated with Lombok Builder.
     * 
     * @return
     */
    public <D> ConfigurableModelMapper withLombokBuilderConverter() {
        mapper.getConfiguration().getConverters().add(0, new LombokBuilderConverter(mapper
                .getConfiguration()));

        return this;
    }

    /*
     * Map destination types having constructor with source type parameter.
     * 
     * @return
     */
    public <D> ConfigurableModelMapper withTypedValueConverter() {
        mapper.getConfiguration().getConverters().add(new TypedValueConverter());

        return this;
    }

    /**
     * Map fields from {@link UUID} to {@link String} and vice versa.
     * 
     * @return
     */
    public ConfigurableModelMapper withUUIDStringConverter() {

        // converter for String -> UUID
        mapper.createTypeMap(String.class, UUID.class)
                .setConverter(context -> UUID.fromString(context.getSource()));

        // converter for UUID -> String
        mapper.createTypeMap(UUID.class, String.class)
                .setConverter(context -> context.getSource().toString());

        return this;

    }

    /**
     * Map fields from {@link OffsetDateTime} to {@link String} and vice versa.
     * 
     * @return
     */
    @SuppressWarnings("null")
    public ConfigurableModelMapper withOffsetDateTimeStringConverter() {

        mapper.getConfiguration().getConverters().add(0,
                new ConditionalConverter<String, OffsetDateTime>() {

                    @Override
                    public OffsetDateTime convert(MappingContext<String, OffsetDateTime> context) {
                        return context
                                .getSource() == null ? null
                                        : OffsetDateTime.ofInstant(Instant.from(
                                                DateTimeFormatter.ISO_OFFSET_DATE_TIME.parse(context
                                                        .getSource())),
                                                ZoneId.of("UTC"));
                    }

                    @Override
                    public MatchResult match(Class<?> sourceType, Class<?> destinationType) {
                        return sourceType.isAssignableFrom(String.class) && destinationType
                                .isAssignableFrom(OffsetDateTime.class) ? MatchResult.FULL
                                        : MatchResult.NONE;
                    }
                });

        mapper.createTypeMap(OffsetDateTime.class, String.class).setConverter(context -> context
                .getSource() == null ? null
                        : DateTimeFormatter.ISO_OFFSET_DATE_TIME.format(context.getSource()
                                .atZoneSameInstant(ZoneId.of("UTC"))));
        return this;

    }

    /**
     * Matching strategy for exact field matching (matching fields by name)
     * 
     * @return
     */
    public ConfigurableModelMapper withMatchingStrategyStrict() {
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        return this;
    }

    /**
     * Matching strategy for non-exact field matching (matching fields by types)
     * 
     * @return
     */
    public ConfigurableModelMapper withMatchingStrategyLoose() {
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);

        return this;
    }

    /**
     * Configuration to get and set fields directly without need of getter or
     * setter
     * 
     * @return
     */
    public ConfigurableModelMapper withFieldMatching() {
        mapper.getConfiguration().setFieldMatchingEnabled(true).setFieldAccessLevel(
                Configuration.AccessLevel.PRIVATE);

        return this;
    }

    /**
     * Provider to be able to construct destination objects do not have a
     * NoArgsConstructor. This Provider needs field matching enabled, due to be
     * able to set private final fields (which haven't a setter)
     * 
     * @return
     */
    public ConfigurableModelMapper withReqArgsConstructorProvider() {
        ReqArgsConstructorProvider provider = new ReqArgsConstructorProvider(mapper);
        // withFieldMatching();
        mapper.getConfiguration().setProvider(request -> {
            mapper.getConfiguration().setDestinationNamingConvention(
                    NamingConventions.JAVABEANS_MUTATOR);
            return provider.get(request);
        });

        return this;
    }
}
