package org.kilrath.modelmapper.converter;

import static java.util.Objects.requireNonNull;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.modelmapper.internal.util.Primitives;
import org.modelmapper.spi.ConditionalConverter;
import org.modelmapper.spi.MappingContext;

public class NumberStringConverter implements ConditionalConverter<Number, String> {

    private DecimalFormat formatter;

    public NumberStringConverter() {
        this(getDefaultDecimalFormat());
    }

    public NumberStringConverter(DecimalFormat format) {
        this.formatter = requireNonNull(format);
    }

    @Override
    public MatchResult match(Class<?> sourceType, Class<?> destinationType) {
        return Number.class.isAssignableFrom(Primitives.wrapperFor(sourceType))
                && String.class.isAssignableFrom(Primitives.wrapperFor(
                        destinationType)) ? MatchResult.FULL
                                : MatchResult.NONE;
    }

    @Override
    public String convert(MappingContext<Number, String> context)
            throws RoundingNotAllowedException {
        try {
            return formatter.format(context.getSource());
        } catch (ArithmeticException e) {
            throw new RoundingNotAllowedException(formatter.getMaximumFractionDigits(), context
                    .getSource(), e);
        } catch (NullPointerException e) {
            return null;
        }
    }

    private static DecimalFormat getDefaultDecimalFormat() {
        DecimalFormat df = new DecimalFormat();
        df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(new Locale("EN", "gb")));
        df.setMaximumFractionDigits(6);
        df.setMinimumFractionDigits(2);
        df.setParseBigDecimal(true);
        df.setGroupingUsed(false);
        df.setRoundingMode(RoundingMode.UNNECESSARY);
        return df;
    }
}
