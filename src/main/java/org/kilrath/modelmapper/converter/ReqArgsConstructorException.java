package org.kilrath.modelmapper.converter;

public class ReqArgsConstructorException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public ReqArgsConstructorException(Exception e) {
        super(e);
    }

    public ReqArgsConstructorException(String msg, Exception e) {
        super(msg, e);
    }
}
