package org.kilrath.modelmapper.converter;

import static java.util.Objects.requireNonNull;

import java.lang.annotation.Annotation;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.modelmapper.Provider;
import org.modelmapper.TypeMap;
import org.modelmapper.internal.Errors;
import org.modelmapper.spi.ConditionalConverter;
import org.modelmapper.spi.ConditionalConverter.MatchResult;
import org.modelmapper.spi.MappingContext;
import org.modelmapper.spi.NamingConvention;
import org.modelmapper.spi.PropertyInfo;
import org.modelmapper.spi.PropertyType;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class ReqArgsConstructorProvider implements Provider<Object> {

    private static final String ACCESSOR_PREFIX = "get";

    private final ExplicitMappingBuilder explicitMappingBuilder;

    private final ModelMapper modelMapper;

    @SuppressWarnings("rawtypes")
    private final Converter unmappablePropertiesConverter;

    private final TypeChecker typeChecker;

    public ReqArgsConstructorProvider(@NotNull ModelMapper mapper) {
        this.modelMapper = mapper;
        this.explicitMappingBuilder = new ExplicitMappingBuilder();
        this.unmappablePropertiesConverter = new UnmappablePropertiesConverter();
        this.typeChecker = new TypeChecker();
    }

    @Override
    public Object get(@NotNull ProvisionRequest<Object> request) {
        Object source = request.getSource();

        Class<?> destType = request.getRequestedType();

        if (source != null) {
            if (typeChecker.hasAReqArgsConstructorOnly(destType)) {
                return createInstance(destType, source);
            } else if (propertiesHaveReqArgsConstructor(destType)
                    && !converterExists(source.getClass(), destType)) {
                createTypeMapWithUnmappablePropertiesConverter(source, destType);
            }
        }
        return null;
    }

    private <S, D> D createInstance(@NotNull Class<D> destinationType, @NotNull S source) {

        if (converterExists(source.getClass(), destinationType)) {
            return modelMapper.map(source, destinationType);
        } else if (typeChecker.hasAReqArgsConstructorOnly(destinationType)) {
            return explicitMappingBuilder.map(source, destinationType);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private <S, D> TypeMap<S, D> createTypeMapWithUnmappablePropertiesConverter(S source,
            Class<D> destType) {

        TypeMap<? extends Object, D> typeMap = modelMapper.getTypeMap(source.getClass(), destType);
        if (typeMap != null) {
            return typeMap.setPostConverter(unmappablePropertiesConverter);
        }

        return null;
    }

    private static void setAccessible(AccessibleObject aObject) {
        if (!aObject.isAccessible()) {
            aObject.setAccessible(true);
        }
    }

    private <D> boolean propertiesHaveReqArgsConstructor(Class<D> type) {
        return Arrays.stream(type.getDeclaredFields())
                .anyMatch(field -> typeChecker.hasAReqArgsConstructorOnly(field.getType()));
    }

    private <S, D> boolean converterExists(Class<S> sourceType, Class<D> destinationType) {
        Optional<ConditionalConverter<?, ?>> suppotedConverter = modelMapper.getConfiguration()
                .getConverters().stream().filter(converter -> !MatchResult.NONE.equals(converter
                        .match(sourceType, destinationType))).findFirst();

        return suppotedConverter.isPresent();
    }

    class ExplicitMappingBuilder {

        @SuppressWarnings("unchecked")
        private <S, D> D map(S source, Class<D> destinationType) {
            // sort all declared constructors in destinationType by
            // parametercount in descending order
            List<Constructor<?>> constructors = Arrays.asList(destinationType
                    .getDeclaredConstructors());
            constructors.sort((Constructor<?> c1, Constructor<?> c2) -> c2.getParameterCount()
                    - c1.getParameterCount());

            for (Constructor<?> reqArgConstr : constructors) {

                List<Object> mappedParameters = Lists.newArrayList();
                try {
                    // make current constructor accessible by reflection
                    setAccessible(reqArgConstr);

                    int j = 0;
                    final Map<Method, PropertyInfo> reqArgsProperties = getMatchedProperties(
                            source.getClass(), destinationType, field -> true);
                    for (Entry<Method, PropertyInfo> reqArgsProperty : reqArgsProperties
                            .entrySet()) {
                        Object sourceValue = reqArgsProperty.getKey().invoke(source);
                        Type parameterType = reqArgsProperty.getValue().getGenericType();
                        Object mappedObject = null;

                        if (reqArgsProperty.getKey().getReturnType().equals(
                                parameterType)) {
                            mappedObject = sourceValue;
                        } else {
                            mappedObject = modelMapper.map(sourceValue, parameterType);
                        }

                        // match found
                        if (mappedObject != null) {
                            Class<?> constrParameterType = reqArgConstr.getParameterTypes()[j];
                            if (mappedObject.getClass().isAssignableFrom(constrParameterType)
                                    || constrParameterType.isAssignableFrom(mappedObject
                                            .getClass())) {
                                j++;
                                mappedParameters.add(mappedObject);
                            }
                        }
                    }
                    if (mappedParameters.size() == reqArgConstr.getParameterCount()) {
                        return (D) reqArgConstr.newInstance(mappedParameters.toArray());
                    }
                } catch (Exception e) {
                    // try next constructor
                }
            }
            return null;
        }

        private <S, D> Map<Method, PropertyInfo> getMatchedProperties(Class<S> sourceType,
                Class<D> destType, Predicate<Field> propertyFilter) {
            return getMatchedProperties(sourceType, destType, propertyFilter,
                    modelMapper.getConfiguration().getSourceNamingConvention());
        }

        private <S, D> Map<Method, PropertyInfo> getMatchedProperties(
                Class<S> sourceType, Class<D> destType, Predicate<Field> propertyFilter,
                NamingConvention methodFilter) {

            Map<String, Method> sourceGetterMap = Arrays.stream(sourceType.getDeclaredMethods())
                    .filter(method -> methodFilter.applies(method.getName(), PropertyType.METHOD))
                    .collect(Collectors.toMap(k -> k.getName().replaceFirst(ACCESSOR_PREFIX, "")
                            .toLowerCase(), v -> v, (duplicateKey1,
                                    duplicateKey2) -> duplicateKey1));

            Map<String, PropertyInfo> destPropertiesMap = getPropertyInfos(destType,
                    propertyFilter);

            Map<Method, PropertyInfo> matchedProperties = Maps.newLinkedHashMap();
            for (Entry<String, PropertyInfo> destinationProperty : destPropertiesMap.entrySet()) {
                if (sourceGetterMap.containsKey(destinationProperty.getKey())) {
                    Method sourceAccessor = sourceGetterMap.get(destinationProperty.getKey());
                    matchedProperties.put(sourceAccessor, destinationProperty.getValue());
                }
            }
            return matchedProperties;
        }

        private <D> Map<String, PropertyInfo> getPropertyInfos(Class<D> type,
                Predicate<Field> propertyFilter) {
            return Arrays.stream(type.getDeclaredFields())
                    .filter(propertyFilter)
                    .collect(Collectors.toMap(k -> k.getName().toLowerCase(),
                            v -> new PropertyInfoImpl(v),
                            (u, v) -> u, LinkedHashMap::new));
        }
    }

    class UnmappablePropertiesConverter implements ConditionalConverter<Object, Object> {

        @Override
        public Object convert(MappingContext<Object, Object> context) {

            final Map<Method, PropertyInfo> reqArgsProperties = explicitMappingBuilder
                    .getMatchedProperties(context.getSourceType(), context.getDestinationType(),
                            field -> typeChecker.hasAReqArgsConstructorOnly(field.getType()));
            for (Entry<Method, PropertyInfo> reqArgsProperty : reqArgsProperties.entrySet()) {
                ProvisionRequest<Object> request = new ProvisionRequest<Object>() {

                    @Override
                    public Object getSource() {
                        try {
                            return reqArgsProperty.getKey().invoke(context.getSource());
                        } catch (IllegalAccessException | IllegalArgumentException
                                | InvocationTargetException e) {
                            throw new ReqArgsConstructorException(
                                    "Could not retrieve property source value!", e);
                        }
                    }

                    @Override
                    @SuppressWarnings("unchecked")
                    public Class<Object> getRequestedType() {
                        return (Class<Object>) reqArgsProperty.getValue().getGenericType();
                    }
                };
                Object mappedProperty = get(request);
                Field destField;
                try {
                    destField = context.getDestinationType().getDeclaredField(reqArgsProperty
                            .getValue().getName());
                    setAccessible(destField);
                    destField.set(context.getDestination(), mappedProperty);
                } catch (NoSuchFieldException | SecurityException | IllegalArgumentException
                        | IllegalAccessException e) {
                    throw new ReqArgsConstructorException(
                            "Could not set property value into destination object!", e);
                }
            }

            return context.getDestination();
        }

        @Override
        public MatchResult match(Class<?> sourceType, Class<?> destinationType) {
            return MatchResult.FULL;
        }
    }

    class TypeChecker {

        boolean hasAReqArgsConstructorOnly(Class<?> type) {
            return !type.isEnum() && !type.isPrimitive() && !isJavaType(type)
                    && !hasALombokBuilder(type) && !hasANoArgsConstructor(type);
        }

        <D> boolean isJavaType(Class<D> type) {
            return type.getPackage() != null && type.getPackage().getName().startsWith(
                    "java");
        }

        <D> boolean hasALombokBuilder(Class<D> destinationType) {
            return getDestinationBuilderClass(destinationType).isPresent();
        }

        <D> boolean hasANoArgsConstructor(Class<D> type) {
            Constructor<?>[] constructors = type.getDeclaredConstructors();
            return constructors.length > 0 && Arrays.stream(constructors).anyMatch(
                    constructor -> constructor.getParameterCount() == 0);
        }

        <D> Optional<Class<?>> getDestinationBuilderClass(Class<D> destinationType) {

            Optional<Class<?>> builderClass = Arrays.stream(destinationType
                    .getDeclaredClasses()).filter(dClass -> containsBuildMethod(dClass
                            .getDeclaredMethods(), destinationType))
                    .findFirst();
            return builderClass;
        }

        <D> boolean containsBuildMethod(Method[] methods, Class<D> destinationType) {
            return Arrays.stream(methods).anyMatch(method -> method.getReturnType().getName()
                    .equals(destinationType.getName()));
        }
    }

    class PropertyInfoImpl implements PropertyInfo {

        private Field member;

        PropertyInfoImpl(Field member) {
            this.member = requireNonNull(member);
        }

        @Override
        public Type getGenericType() {
            return member.getGenericType();
        }

        <S> Object getValue(S subject) {
            try {
                return member.get(subject);
            } catch (Exception e) {
                throw new Errors().errorGettingValue(getMember(), e).toMappingException();
            }
        }

        @Override
        public <T extends Annotation> T getAnnotation(Class<T> annotationClass) {
            return member.getAnnotation(annotationClass);
        }

        @Override
        public Class<?> getInitialType() {
            return member.getClass();
        }

        @Override
        public Member getMember() {
            return member;
        }

        @Override
        public String getName() {
            return member.getName();
        }

        @Override
        public PropertyType getPropertyType() {
            return PropertyType.FIELD;
        }

        @Override
        public Class<?> getType() {
            return member.getType();
        }

        @Override
        public String toString() {
            return "PropertyInfoImpl [member=" + member + "]";
        }
    }
}
