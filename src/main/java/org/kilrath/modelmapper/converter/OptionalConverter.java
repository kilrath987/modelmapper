package org.kilrath.modelmapper.converter;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;

import org.modelmapper.TypeToken;
import org.modelmapper.internal.MappingEngineImpl;
import org.modelmapper.internal.util.Types;
import org.modelmapper.spi.ConditionalConverter;
import org.modelmapper.spi.Mapping;
import org.modelmapper.spi.MappingContext;
import org.modelmapper.spi.PropertyInfo;
import org.modelmapper.spi.PropertyMapping;

@SuppressWarnings("rawtypes")
public class OptionalConverter implements ConditionalConverter<Object, Object> {

    @Override
    public MatchResult match(Class<?> sourceType, Class<?> destinationType) {
        if (Optional.class.isAssignableFrom(sourceType) || Optional.class.isAssignableFrom(
                destinationType)) {
            return MatchResult.FULL;
        } else {
            return MatchResult.NONE;
        }
    }

    private Type getElementType(MappingContext<Object, Object> context) {
        Mapping mapping = context.getMapping();
        if (mapping instanceof PropertyMapping) {
            PropertyInfo destInfo = ((PropertyMapping) mapping).getLastDestinationProperty();
            return getElementType(destInfo.getGenericType());
        } else {
            return getElementType(context.getGenericDestinationType());
        }
    }

    private Type getElementType(Type type) {
        if (Optional.class.isAssignableFrom(Types.rawTypeFor(type))) {
            return ((ParameterizedType) type).getActualTypeArguments()[0];
        } else {
            return type;
        }
    }

    @Override
    public Object convert(MappingContext<Object, Object> context) {
        Object dest = map(context);

        if (Optional.class.isAssignableFrom(context.getDestinationType())) {
            return Optional.ofNullable(dest);
        }

        return dest;
    }

    @SuppressWarnings("unchecked")
    private <S, D> Object map(MappingContext<Object, Object> context) {
        S source = extractOptional(context.getSource());
        D destination = null;
        Type destType = getElementType(context);

        if (source != null) {
            Class<S> sourceType = (Class<S>) source.getClass();
            TypeToken<D> destinationTypeToken = TypeToken.of(destType);
            MappingEngineImpl mappingEngine = (MappingEngineImpl) context.getMappingEngine();
            String typeMapName = sourceType.getSimpleName() + " -> " + destinationTypeToken
                    .getRawType().getSimpleName();

            destination = mappingEngine.map(source, sourceType, destination, destinationTypeToken,
                    typeMapName);
        }
        return destination;
    }

    @SuppressWarnings("unchecked")
    private <S> S extractOptional(Object optObject) {
        if (optObject instanceof Optional) {
            Optional optSource = (Optional) optObject;
            if (optSource.isPresent()) {
                return ((S) optSource.get());
            } else {
                return null;
            }
        }
        return (S) optObject;
    }
}
