package org.kilrath.modelmapper.converter;

public class RoundingNotAllowedException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public RoundingNotAllowedException(int maxFractionDigitsAllowed, Number errornousValue,
            ArithmeticException e) {
        super("Value [" + errornousValue.toString() + "] exceeds maxFractionDigits "
                + maxFractionDigitsAllowed, e);
    }

}