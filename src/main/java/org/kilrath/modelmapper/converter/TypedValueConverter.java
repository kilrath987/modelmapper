package org.kilrath.modelmapper.converter;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

import org.modelmapper.spi.ConditionalConverter;
import org.modelmapper.spi.MappingContext;

public class TypedValueConverter implements ConditionalConverter<Object, Object> {

    @Override
    public MatchResult match(Class<?> sourceType, Class<?> destinationType) {

        if (isSourceTypeInDestinationType(sourceType, destinationType)
                || sourceTypeContainsDestinationType(sourceType, destinationType)) {
            return MatchResult.FULL;
        } else {
            return MatchResult.NONE;
        }
    }

    private boolean isSourceTypeInDestinationType(Class<?> sourceType, Class<?> destinationType) {
        try {
            return destinationType.getDeclaredConstructor(sourceType) != null;
        } catch (NoSuchMethodException | SecurityException e) {
            return false;
        }
    }

    private boolean sourceTypeContainsDestinationType(Class<?> sourceType,
            Class<?> destinationType) {
        try {
            return sourceType.getDeclaredFields().length == 1 && sourceType.getDeclaredFields()[0]
                    .getType().equals(destinationType);
        } catch (SecurityException e) {
            return false;
        }
    }

    @Override
    public Object convert(MappingContext<Object, Object> context) {

        Object source = context.getSource();
        Class<Object> destType = context.getDestinationType();

        Object destination = null;
        if (isSourceTypeInDestinationType(source.getClass(), destType)) {
            destination = createNewInstance(source, destType);
        } else {
            destination = extractValueFromSource(source);
        }

        return destination;
    }

    private Object extractValueFromSource(Object source) {
        Object destination = null;

        Field destField = source.getClass().getDeclaredFields()[0];
        if (!destField.isAccessible()) {
            destField.setAccessible(true);
        }
        try {
            destination = destField.get(source);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return destination;
    }

    private Object createNewInstance(Object source, Class<Object> destType) {
        Object destination = null;
        try {
            Constructor<Object> constructor = destType.getDeclaredConstructor(source.getClass());
            if (!constructor.isAccessible()) {
                constructor.setAccessible(true);
            }
            destination = constructor.newInstance(source);
        } catch (NoSuchMethodException | SecurityException | InstantiationException
                | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return destination;
    }
}