package org.kilrath.modelmapper.converter;

import static java.util.Objects.requireNonNull;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParsePosition;
import java.util.Locale;

import org.modelmapper.internal.Errors;
import org.modelmapper.internal.util.Primitives;
import org.modelmapper.spi.ConditionalConverter;
import org.modelmapper.spi.MappingContext;

public class NumberConverter implements ConditionalConverter<Object, Number> {

    private DecimalFormat formatter;

    public NumberConverter() {
        this(getDefaultDecimalFormat());
    }

    public NumberConverter(DecimalFormat format) {
        this.formatter = requireNonNull(format);
    }

    @Override
    public MatchResult match(Class<?> sourceType, Class<?> destinationType) {

        return (Number.class.isAssignableFrom(Primitives.wrapperFor(sourceType))
                || String.class.isAssignableFrom(Primitives.wrapperFor(sourceType)))
                && Number.class.isAssignableFrom(Primitives.wrapperFor(
                        destinationType)) ? MatchResult.FULL
                                : MatchResult.NONE;
    }

    @Override
    public Number convert(MappingContext<Object, Number> context) {
        Object source = context.getSource();
        if (source == null)
            return null;

        Class<?> destinationType = Primitives.wrapperFor(context.getDestinationType());

        if (source instanceof Number)
            return numberFor((Number) source, destinationType);

        return numberFor(source.toString(), destinationType);
    }

    /**
     * Creates a Number for the {@code source} and {@code destinationType}.
     * 
     * @param source
     * @param destinationType
     * @return
     */
    Number numberFor(Number source, Class<?> destinationType) {
        if (destinationType.equals(source.getClass()))
            return source;

        if (destinationType.equals(Byte.class)) {
            long longValue = source.longValue();
            if (longValue > Byte.MAX_VALUE)
                throw new Errors().errorTooLarge(source, destinationType).toMappingException();
            if (longValue < Byte.MIN_VALUE)
                throw new Errors().errorTooSmall(source, destinationType).toMappingException();
            return Byte.valueOf(source.byteValue());
        }

        if (destinationType.equals(Short.class)) {
            long longValue = source.longValue();
            if (longValue > Short.MAX_VALUE)
                throw new Errors().errorTooLarge(source, destinationType).toMappingException();
            if (longValue < Short.MIN_VALUE)
                throw new Errors().errorTooSmall(source, destinationType).toMappingException();
            return Short.valueOf(source.shortValue());
        }

        if (destinationType.equals(Integer.class)) {
            long longValue = source.longValue();
            if (longValue > Integer.MAX_VALUE)
                throw new Errors().errorTooLarge(source, destinationType).toMappingException();
            if (longValue < Integer.MIN_VALUE)
                throw new Errors().errorTooSmall(source, destinationType).toMappingException();
            return Integer.valueOf(source.intValue());
        }

        if (destinationType.equals(Long.class))
            return Long.valueOf(source.longValue());

        if (destinationType.equals(Float.class)) {
            if (source.doubleValue() > Float.MAX_VALUE)
                throw new Errors().errorTooLarge(source, destinationType).toMappingException();
            return Float.valueOf(source.floatValue());
        }

        if (destinationType.equals(Double.class))
            return Double.valueOf(source.doubleValue());

        if (destinationType.equals(BigDecimal.class)) {
            if (source instanceof Float || source instanceof Double)
                return new BigDecimal(source.toString());
            else if (source instanceof BigInteger)
                return new BigDecimal((BigInteger) source);
            else
                return BigDecimal.valueOf(source.longValue());
        }

        if (destinationType.equals(BigInteger.class)) {
            if (source instanceof BigDecimal)
                return ((BigDecimal) source).toBigInteger();
            else
                return BigInteger.valueOf(source.longValue());
        }

        throw new Errors().errorMapping(source, destinationType).toMappingException();
    }

    /**
     * Creates a Number for the {@code source} and {@code destinationType}.
     * 
     * @param source
     * @param destinationType
     * @return
     */
    Number numberFor(String source, Class<?> destinationType) {
        String sourceString = source.trim();
        if (sourceString.length() == 0)
            return null;

        try {
            if (destinationType.equals(Byte.class))
                return Byte.valueOf(sourceString);
            if (destinationType.equals(Short.class))
                return Short.valueOf(sourceString);
            if (destinationType.equals(Integer.class))
                return Integer.valueOf(sourceString);
            if (destinationType.equals(Long.class))
                return Long.valueOf(sourceString);
            if (destinationType.equals(Float.class))
                return Float.valueOf(sourceString);
            if (destinationType.equals(Double.class)) {
                return Double.valueOf(sourceString);
            }
            if (destinationType.equals(BigDecimal.class)) {
                formatter.setParseBigDecimal(true);
                return formatter.parse(sourceString, new ParsePosition(0));
            }
            if (destinationType.equals(BigInteger.class))
                return new BigInteger(sourceString);
        } catch (Exception e) {
            throw new Errors().errorMapping(sourceString, destinationType, e).toMappingException();
        }

        throw new Errors().errorMapping(sourceString, destinationType).toMappingException();
    }

    private static DecimalFormat getDefaultDecimalFormat() {
        DecimalFormat df = new DecimalFormat();
        df.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(new Locale("EN", "gb")));
        df.setMaximumFractionDigits(6);
        df.setMinimumFractionDigits(2);
        df.setParseBigDecimal(true);
        df.setGroupingUsed(false);
        df.setRoundingMode(RoundingMode.UNNECESSARY);
        return df;
    }
}
