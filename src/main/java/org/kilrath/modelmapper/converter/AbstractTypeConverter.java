package org.kilrath.modelmapper.converter;

import static java.util.Objects.requireNonNull;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.kilrath.modelmapper.annotation.TypeField;
import org.modelmapper.config.Configuration;
import org.modelmapper.config.Configuration.AccessLevel;
import org.modelmapper.internal.util.Types;
import org.modelmapper.spi.ConditionalConverter;
import org.modelmapper.spi.MappingContext;

import io.github.lukehutch.fastclasspathscanner.FastClasspathScanner;

public class AbstractTypeConverter implements ConditionalConverter<Object, Object> {

    private String classpathPrefix = "";

    private Configuration configuration;

    public AbstractTypeConverter(@NotNull String classpathPrefix, Configuration configuration) {
        this.configuration = requireNonNull(configuration);
        this.classpathPrefix = requireNonNull(classpathPrefix);

    }

    @Override
    public MatchResult match(Class<?> sourceType, Class<?> destinationType) {

        if (Modifier.isAbstract(destinationType.getModifiers())
                && !Collection.class.isAssignableFrom(destinationType)
                && !Map.class.isAssignableFrom(destinationType)
                && !destinationType.isPrimitive()
                && !destinationType.isArray()
                && !destinationType.isEnum()) {
            return MatchResult.FULL;
        } else {

            return MatchResult.NONE;
        }
    }

    private Class<?> getElementType(MappingContext<Object, Object> context) {

        Class<Object> destType = context.getDestinationType();
        String srcTypeName = context.getSourceType().getSimpleName();

        List<String> resultList = new ArrayList<>();
        FastClasspathScanner scanner = new FastClasspathScanner(classpathPrefix);
        if (destType.isInterface()) {
            scanner.matchClassesImplementing(Types.rawTypeFor(destType),
                    e -> resultList.add(e.getName())).scan();
        } else {
            scanner.matchSubclassesOf(Types.rawTypeFor(destType),
                    e -> resultList.add(e.getName())).scan();
        }
        List<Class<?>> subTypesOf = resultList.stream().map(subType -> getClassFor(
                subType)).collect(Collectors.toList());

        Optional<Class<?>> matchingSubType = subTypesOf.stream().filter(subType -> subType
                .getSimpleName().contains(srcTypeName)
                || srcTypeName.contains(subType.getSimpleName())
                || matchTypeFieldAnnotation(subType.getSimpleName(), context
                        .getSource()))
                .findFirst();

        if (matchingSubType.isPresent()) {
            return matchingSubType.get();
        } else if (subTypesOf.size() == 1) {
            return subTypesOf.get(0);
        } else if (context.getDestinationType() != context.getGenericDestinationType()) {
            try {
                return Types.rawTypeFor(context.getGenericDestinationType());
            } catch (IllegalArgumentException e) {
                throw new AbstractTypeMatchingException(destType);
            }
        }
        throw new AbstractTypeMatchingException(destType);
    }

    private boolean matchTypeFieldAnnotation(String type, Object source) {
        Optional<Field> tField = Arrays.stream(source.getClass().getDeclaredFields()).filter(
                field -> field.isAnnotationPresent(TypeField.class)).findFirst();
        if (tField.isPresent()) {
            try {
                Field typeField = tField.get();
                typeField.setAccessible(true);
                String value = typeField.get(source).toString();

                TypeField annotation = typeField.getDeclaredAnnotation(TypeField.class);
                String regex = Arrays.stream(annotation.removeChars()).collect(Collectors.joining(
                        "|", "[", "]"));
                int start = annotation.substringStartIndex();
                start = start < 0 && start > value.length() - 1 ? 0 : start;
                int end = annotation.substringEndIndex();
                end = end > 0 && end < -1 * value.length() ? value.length() - 1
                        : end + value.length();
                String prefix = annotation.prefix();
                String suffix = annotation.suffix();

                String typeValue = prefix + value.substring(start, end).replaceAll(regex, "")
                        + suffix;

                return annotation.caseInsensitive() ? type.equalsIgnoreCase(typeValue)
                        : type.equals(typeValue);
            } catch (Exception e) {
                throw new RuntimeException("Could not get value for a TypeField annotated field"
                        + source.getClass(),
                        e);
            }
        }
        return false;
    }

    private Class<?> getClassFor(String subType) {
        try {
            return Class.forName(subType);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Could not find class for abstract class subtype " + subType,
                    e);
        }
    }

    @Override
    public Object convert(MappingContext<Object, Object> context) {
        Class<?> destType = getElementType(context);

        Object source = context.getSource();
        Object dest = null;
        if (source != null) {
            AccessLevel fieldAccessLevel = configuration.getFieldAccessLevel();
            boolean isfieldMatchingAlreadyEnabled = configuration.isFieldMatchingEnabled();
            boolean hasAFinalField = Arrays.stream(destType.getDeclaredFields()).anyMatch(
                    field -> {
                        if (!field.isAccessible()) {
                            field.setAccessible(true);
                        }
                        return Modifier.isFinal(field.getModifiers());
                    });
            configuration.setFieldMatchingEnabled(hasAFinalField).setFieldAccessLevel(
                    AccessLevel.PRIVATE);

            MappingContext<?, ?> newContext = context.create(source, destType);
            dest = context.getMappingEngine().map(newContext);

            configuration.setFieldMatchingEnabled(isfieldMatchingAlreadyEnabled)
                    .setFieldAccessLevel(fieldAccessLevel);
        }

        return dest;
    }
}
