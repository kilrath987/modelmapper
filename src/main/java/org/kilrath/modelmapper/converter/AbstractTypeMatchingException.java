package org.kilrath.modelmapper.converter;

import java.text.MessageFormat;

import org.kilrath.modelmapper.annotation.TypeField;

public class AbstractTypeMatchingException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * Will be thrown if any abstract source type could be matched to a concrete
     * destination class. Use {@link TypeField} at a source member to define a
     * matching between abstract source type and concrete destination type
     * 
     * @param destType
     */
    public AbstractTypeMatchingException(Class<Object> destType) {
        super(MessageFormat.format(
                "Could not match any subclass / implementing class from abstract type {0}!"
                        + "\nIf there is a field in source object declaring a type, try to use {1}"
                        + " to define concrete destination type", destType, TypeField.class));
    }
}