package org.kilrath.modelmapper.converter;

import static java.util.Objects.requireNonNull;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import org.modelmapper.config.Configuration;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.convention.NamingConventions;
import org.modelmapper.spi.ConditionalConverter;
import org.modelmapper.spi.MappingContext;
import org.modelmapper.spi.MatchingStrategy;
import org.modelmapper.spi.NamingConvention;
import org.modelmapper.spi.PropertyType;

public class LombokBuilderConverter implements ConditionalConverter<Object, Object> {

    private Configuration configuration;

    public LombokBuilderConverter(@NotNull Configuration configuration) {
        this.configuration = requireNonNull(configuration);
    }

    @Override
    public MatchResult match(Class<?> sourceType, Class<?> destinationType) {

        Optional<Class<?>> builderClass = getDestinationBuilderClass(sourceType, destinationType);

        if (builderClass.isPresent()) {
            return MatchResult.FULL;
        } else {
            return MatchResult.NONE;
        }
    }

    @SuppressWarnings("null")
    @Override
    public Object convert(MappingContext<Object, Object> context) {

        Object source = context.getSource();
        Class<Object> sourceType = context.getSourceType();
        Class<Object> destType = context.getDestinationType();
        Optional<Class<?>> builderClass = getDestinationBuilderClass(sourceType, destType);
        Object destination = null;

        if (source != null) {
            MatchingStrategy currentMatchingStrategy = configuration.getMatchingStrategy();
            configuration.setMatchingStrategy(MatchingStrategies.STRICT);
            configuration.setDestinationNamingConvention(LombokBuilderNamingConvention.INSTANCE);

            MappingContext<?, ?> newContext = context.create(source, builderClass.get());

            destination = context.getMappingEngine().map(newContext);

            destination = invokeBuildMethodOn(destination, destType.getSimpleName());

            configuration.setMatchingStrategy(currentMatchingStrategy);
            configuration.setDestinationNamingConvention(NamingConventions.JAVABEANS_MUTATOR);
        }

        return destination;
    }

    private Optional<Class<?>> getDestinationBuilderClass(Class<?> sourceType,
            Class<?> destinationType) {

        if (!(Optional.class.isAssignableFrom(sourceType))) {
            Optional<Class<?>> builderClass = Arrays.stream(destinationType
                    .getDeclaredClasses()).filter(dClass -> containsBuildMethod(dClass
                            .getDeclaredMethods(), destinationType))
                    .findFirst();
            return builderClass;
        }

        return Optional.empty();
    }

    private boolean containsBuildMethod(Method[] methods, Class<?> destinationType) {
        return Arrays.stream(methods).anyMatch(method -> method.getReturnType().getName().equals(
                destinationType.getName()));
    }

    private Object invokeBuildMethodOn(Object result, String targetClassName) {

        final Optional<Method> buildMethod = Arrays.stream(result.getClass()
                .getMethods()).filter(bMethod -> bMethod.getReturnType()
                        .getSimpleName()
                        .equals(targetClassName))
                .findFirst();

        if (buildMethod.isPresent()) {

            try {
                return buildMethod.get().invoke(result);
            } catch (IllegalAccessException | IllegalArgumentException
                    | InvocationTargetException e) {
                throw new RuntimeException(MessageFormat.format(
                        "Could not invoke build method in class {0} to create an instance of {1}. Required fields could not be mapped: {2}",
                        result.getClass().getSimpleName(), targetClassName, result.toString()), e);
            }
        }
        return result;
    }
}

class LombokBuilderNamingConvention implements NamingConvention {

    public static final LombokBuilderNamingConvention INSTANCE = new LombokBuilderNamingConvention();

    @Override
    public boolean applies(String propertyName, PropertyType propertyType) {
        return PropertyType.METHOD.equals(propertyType);
    }

    @Override
    public String toString() {
        return "Lombok @Builder Naming Convention";
    }
}
