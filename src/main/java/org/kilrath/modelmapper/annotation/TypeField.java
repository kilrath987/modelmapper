package org.kilrath.modelmapper.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Defines how to interprete to value of the field to identify a target type for
 * mapping
 * 
 * @author Johannes.Kozakiewicz
 *
 * @params <b>removeChars</b> - defines chars that should be completely removed
 *         from value<br>
 *         <b>caseInsensitive</b> - defines if searching for a target type
 *         should be case insensitive or not (default: false) <br>
 *         <b>substringStartIndex</b> - defines the substring start index from
 *         which the value should be interpreted as type (range is positive (num
 *         of letters cutted from begin)) <br>
 *         <b>substringEndIndex</b> - defines the substring end index from which
 *         the value should be interpreted as type (range is negative (num of
 *         letters cutted from end))<br>
 *         <b>prefix</b> - defines a prefix which should be added at the
 *         beginning of the value to identify the target type <br>
 *         <b>suffix</b> - defines a suffix which should be added at the end of
 *         the value to identify the target type
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface TypeField {
    String[] removeChars() default {};

    boolean caseInsensitive() default false;

    int substringStartIndex() default 0;

    int substringEndIndex() default 0;

    String prefix() default "";

    String suffix() default "";
}
