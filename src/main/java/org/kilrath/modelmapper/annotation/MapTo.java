package org.kilrath.modelmapper.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Configures the mapping of one bean attribute or enum constant.
 * <p>
 * The name of the mapped attribute or constant is to be specified via
 * {@link #target()}. For mapped bean attributes it is assumed by default that
 * the attribute has the same name in the source bean. Alternatively, one of
 * {@link #source()} can be specified to define the property source.
 * <p>
 * In addition, the attributes {@link #dateFormat()} may be used to further
 * define the mapping.
 *
 * 
 * @author Johannes.Kozakiewicz
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface MapTo {

    /**
     * The target name of the configured property as defined by the JavaBeans
     * specification. The same target property must not be mapped more than
     * once.
     * <p>
     * If used to map an enum constant, the name of the constant member is to be
     * given. In this case, several values from the source enum may be mapped to
     * the same value of the target enum.
     *
     * @return The target name of the configured property or enum constant
     */
    String target();

    /**
     * A format string as processable by {@link SimpleDateFormat} if the
     * attribute is mapped from {@code String} to {@link Date} or vice-versa.
     * Will be ignored for all other attribute types and when mapping enum
     * constants.
     *
     * @return A date format string as processable by {@link SimpleDateFormat}.
     */
    String dateFormat() default "";

    /**
     * A format string as processable by {@link DecimalFormat} if the annotated
     * method maps from a {@link Number} to a {@link String} or vice-versa. Will
     * be ignored for all other element types.
     *
     * @return A decimal format string as processable by {@link DecimalFormat}.
     */
    String numberFormat() default "";

    /**
     * Whether the property specified via {@link #target()} should be ignored by
     * the generated mapping method or not. This can be useful when certain
     * attributes should not be propagated from source or target or when
     * properties in the target object are populated using a decorator and thus
     * would be reported as unmapped target property by default.
     *
     * @return {@code true} if the given property should be ignored,
     *         {@code false} otherwise
     */
    boolean ignore() default false;

    /**
     * In case the source property is {@code null}, the provided default
     * {@link String} value is set. If the designated target property is not of
     * type {@code String}, the value will be converted by applying a matching
     * conversion method or built-in conversion.
     *
     * @return Default value to set in case the source property is {@code null}.
     */
    String defaultValue() default "";
}
