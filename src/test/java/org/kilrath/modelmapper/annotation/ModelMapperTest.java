package org.kilrath.modelmapper.annotation;
//
// import static org.junit.Assert.assertEquals;
//
// import org.junit.Before;
// import org.junit.Test;
// import org.modelmapper.Provider;
// import org.modelmapper.config.Configuration.AccessLevel;
//
/// **
// * Maps to an immutable destination.
// *
// * @author Jonathan Halterman
// */
// public class ModelMapperTest {
//
// class Source {
// String name;
//
// String address;
// }
//
// class Destination1 {
// private final String name;
//
// private final String address;
//
// Destination1(String name, String address) {
// this.name = name;
// this.address = address;
// }
// }
//
// class Destination2 {
// private final String name;
//
// private final String address;
//
// Destination2(String name, String address) {
// this.name = "";
// this.address = "";
// }
// }
//
// private ModelMapper modelMapper;
//
// @Before
// public void setup() {
// modelMapper = new ConfigurableModelMapper();
// }
//
// @Test
// public void shouldMapToImmutableViaProvider() {
// modelMapper.createTypeMap(Source.class, Destination1.class).setProvider(
// new Provider<Destination1>() {
// @Override
// public Destination1 get(ProvisionRequest<Destination1> request) {
// Source s = Source.class.cast(request.getSource());
// return new Destination1(s.name, s.address);
// }
// });
//
// Source source = new Source();
// source.name = "Joe";
// source.address = "Main Street";
//
// Destination1 dest1 = modelMapper.map(source, Destination1.class);
// assertEquals(dest1.name, "Joe");
// assertEquals(dest1.address, "Main Street");
// }
//
// @Test
// public void shouldMapToImmutableViaSecurityOverride() {
// modelMapper.getConfiguration()
// .setFieldMatchingEnabled(true)
// .setFieldAccessLevel(AccessLevel.PRIVATE);
//
// Source source = new Source();
// source.name = "Joe";
// source.address = "Main Street";
//
// Destination2 dest2 = modelMapper.map(source, Destination2.class);
// assertEquals(dest2.name, "Joe");
// assertEquals(dest2.address, "Main Street");
// }
// }