package org.kilrath.modelmapper;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

public class TestSource {

    private Optional<BigDecimal> amount;

    private String currency;

    private Optional<String> fraction;

    private Optional<List<Integer>> numbers;

    private Map<UUID, String> idMap;

    public TestSource(Optional<BigDecimal> amount, String currency, Optional<String> fraction,
            Optional<List<Integer>> numbers, Map<UUID, String> idMap) {
        this.amount = amount;
        this.currency = currency;
        this.fraction = fraction;
        this.numbers = numbers;
        this.idMap = idMap;

    }

    public Optional<BigDecimal> getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public Optional<String> getFraction() {
        return fraction;
    }

    public Optional<List<Integer>> getNumbers() {
        return numbers;
    }

    public Map<UUID, String> getIdMap() {
        return idMap;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((amount == null) ? 0 : amount.hashCode());
        result = prime * result + ((currency == null) ? 0 : currency.hashCode());
        result = prime * result + ((fraction == null) ? 0 : fraction.hashCode());
        result = prime * result + ((idMap == null) ? 0 : idMap.hashCode());
        result = prime * result + ((numbers == null) ? 0 : numbers.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        TestSource other = (TestSource) obj;
        if (amount == null) {
            if (other.amount != null)
                return false;
        } else if (!amount.equals(other.amount))
            return false;
        if (currency == null) {
            if (other.currency != null)
                return false;
        } else if (!currency.equals(other.currency))
            return false;
        if (fraction == null) {
            if (other.fraction != null)
                return false;
        } else if (!fraction.equals(other.fraction))
            return false;
        if (idMap == null) {
            if (other.idMap != null)
                return false;
        } else if (!idMap.equals(other.idMap))
            return false;
        if (numbers == null) {
            if (other.numbers != null)
                return false;
        } else if (!numbers.equals(other.numbers))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "TestSource [amount=" + amount + ", currency=" + currency + ", fraction=" + fraction
                + ", numbers=" + numbers + ", idMap=" + idMap + "]";
    }
}
