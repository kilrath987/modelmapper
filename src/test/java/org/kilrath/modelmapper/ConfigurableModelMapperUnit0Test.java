package org.kilrath.modelmapper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.reflect.TypeToken;

public class ConfigurableModelMapperUnit0Test {

    public ConfigurableModelMapper modelMapper;

    @Before
    public void setup() {
        modelMapper = new ConfigurableModelMapper()
                .withNumberConverter(Optional.empty())
                .withOffsetDateTimeStringConverter()
                .withOptionalConverter()
                .withAbstractTypeConverter("")
                .withLombokBuilderConverter()
                .withTypedValueConverter()
                .withMatchingStrategyStrict()
                .withReqArgsConstructorProvider();
    }

    @Test
    public void testComplexSourceAndDestination() {
        TestSource source;
        source = new TestSource(Optional.empty(), null, Optional.empty(), Optional.of(Lists
                .newArrayList()), Maps.newHashMap());
        TestDestination result3 = modelMapper.map(source, TestDestination.class);
        assertNull(result3.getAmount());
        assertFalse(result3.getCurrency().isPresent());
        assertFalse(result3.getFraction().isPresent());
        assertTrue(result3.getNumbers().isEmpty());
        assertTrue(result3.getIdMap().isEmpty());
    }

    @Test
    @SuppressWarnings({ "boxing" })
    public void testComplexSourceToOptionalDestination() {
        TypeToken<Optional<TestDestination>> optionalAmountPMO = new TypeToken<Optional<TestDestination>>() {
            public static final long serialVersionUID = 1L;
            /**/};

        HashMap<UUID, String> idMap = Maps.newHashMap();
        UUID uuid = UUID.randomUUID();
        idMap.put(uuid, "1");
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5);
        TestSource source = new TestSource(Optional.of(BigDecimal.TEN), "EUR", Optional.of("2.00"),
                Optional.of(numbers), idMap);

        Optional<TestDestination> result2 = modelMapper.map(source, optionalAmountPMO.getType());

        assertEquals("10.00", result2.get().getAmount());
        assertEquals("EUR", result2.get().getCurrency().get());
        assertFalse(String.class.isAssignableFrom(result2.get().getFraction().get().getClass()));
        assertEquals(Double.valueOf(2.0), result2.get().getFraction().get());
        assertTrue(result2.get().getNumbers().size() == 5);
        assertThat(result2.get().getNumbers()).allMatch(number -> numbers.contains(number
                .intValue()));
        assertTrue(result2.get().getIdMap().size() == 1);
        assertEquals(1, result2.get().getIdMap().get(uuid.toString()).intValue());
    }

    @Test
    public void testOptionalSourceAndDestination() {
        Optional<BigDecimal> result1;
        TypeToken<Optional<BigDecimal>> optionalBigDecimal = new TypeToken<Optional<BigDecimal>>() {
            public static final long serialVersionUID = 1L;
            /**/};
        result1 = modelMapper.map(Optional.of(Integer.valueOf(10)), optionalBigDecimal.getType());
        assertTrue(result1.isPresent());
        assertEquals(BigDecimal.TEN, result1.get());
    }

    @Test
    public void testOptionalSourceToBigDecimal() {
        BigDecimal result0;
        result0 = modelMapper.map(Optional.of(Integer.valueOf(10)), BigDecimal.class);
        assertEquals(BigDecimal.TEN, result0);
    }

    @Test
    public void testIntegerToOptionalDestination() {
        Optional<BigDecimal> result1;
        TypeToken<Optional<BigDecimal>> optionalBigDecimal = new TypeToken<Optional<BigDecimal>>() {
            public static final long serialVersionUID = 1L;
            /**/};
        result1 = modelMapper.map(Integer.valueOf(10), optionalBigDecimal.getType());
        assertTrue(result1.isPresent());
        assertEquals(BigDecimal.TEN, result1.get());
    }

    @Test
    public void testIntegerToBigDecimal() {
        BigDecimal result0;
        result0 = modelMapper.map(Integer.valueOf(10), BigDecimal.class);
        assertEquals(BigDecimal.TEN, result0);
    }

    @Test
    public void testOptionalEmptySourceAndDestination() {
        Optional<BigDecimal> result1;
        TypeToken<Optional<BigDecimal>> optionalBigDecimal = new TypeToken<Optional<BigDecimal>>() {
            public static final long serialVersionUID = 1L;
            /**/};
        result1 = modelMapper.map(Optional.empty(), optionalBigDecimal.getType());
        assertFalse(result1.isPresent());
    }

    @Test
    public void testOptionalEmptySource() {
        BigDecimal result0;
        result0 = modelMapper.map(Optional.empty(), BigDecimal.class);
        assertNull(result0);
    }

    @Test
    public void testNullSourceOptionalDestination() {

        TypeToken<Optional<BigDecimal>> optionalBigDecimal = new TypeToken<Optional<BigDecimal>>() {
            public static final long serialVersionUID = 1L;
            /**/};

        Optional<BigDecimal> result1 = modelMapper.map(null, optionalBigDecimal.getType());

        assertFalse(result1.isPresent());
    }

    @Test
    public void testNullSource() {

        BigDecimal result0 = modelMapper.map(null, BigDecimal.class);

        assertNull(result0);
    }

    @Test
    public void testPrimitiveDestination() {

        @SuppressWarnings("boxing")
        int result0 = modelMapper.map(1000, int.class);

        assertEquals(1000, result0);
    }

    private enum TestSourceEnum {
        T1, T2, T3
    }

    private enum TestDestinationEnum {
        T2, T3
    }

    @Test
    public void testEnumSourceAndDestination_NotMapped() {

        TestDestinationEnum result0 = modelMapper.map(TestSourceEnum.T1, TestDestinationEnum.class);

        assertNull(result0);
    }

    @Test
    public void testEnumSourceAndDestination_Mapped() {

        TestDestinationEnum result0 = modelMapper.map(TestSourceEnum.T2, TestDestinationEnum.class);

        assertEquals(TestDestinationEnum.T2, result0);
    }

    @Test
    public void testEnumSourceAndDestination_List() {

        TestSourceEnum[] allValues = TestSourceEnum.values();
        TypeToken<List<TestDestinationEnum>> listTestDestinationEnum = new TypeToken<List<TestDestinationEnum>>() {
            public static final long serialVersionUID = 1L;
            /**/};

        List<TestDestinationEnum> result0 = modelMapper.map(allValues, listTestDestinationEnum
                .getType());

        assertEquals(Lists.newArrayList(null, TestDestinationEnum.T2, TestDestinationEnum.T3),
                result0);
    }

}
