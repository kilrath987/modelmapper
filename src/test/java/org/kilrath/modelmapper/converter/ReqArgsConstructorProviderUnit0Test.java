package org.kilrath.modelmapper.converter;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.kilrath.modelmapper.ConfigurableModelMapper;
import org.kilrath.modelmapper.TestDestination;
import org.kilrath.modelmapper.TestSource;

import com.google.common.collect.Maps;

public class ReqArgsConstructorProviderUnit0Test {

    ConfigurableModelMapper modelMapper;

    @Before
    public void setUp() throws Exception {
        modelMapper = new ConfigurableModelMapper()
                .withReqArgsConstructorProvider()
                .withOptionalConverter();
    }


    @Test
    public void testConversion_isPrimitive() {
        int source = 55;
        BigDecimal result = modelMapper.map(source, BigDecimal.class);

        assertEquals(BigDecimal.valueOf(55), result);
    }

    @Test
    @SuppressWarnings("boxing")
    public void testMap_ObjectWithReqArgsConstructor() throws Exception {
        // arrange
        Optional<BigDecimal> amount = Optional.of(new BigDecimal("0.99"));
        String currency = "USR";
        Optional<String> fraction = Optional.ofNullable("0.09");
        Optional<List<Integer>> numbers = Optional.of(Arrays.asList(1, 2, 3, 4, 5));
        Map<UUID, String> idMap = Maps.newHashMap();
        TestSource source = new TestSource(amount, currency, fraction, numbers, idMap);

        // action
        TestDestination result = modelMapper.map(source, TestDestination.class);

        TestSource backAgainModel = modelMapper.map(result, TestSource.class);

        // assert
        assertThat(backAgainModel).isEqualTo(source);
    }
}
